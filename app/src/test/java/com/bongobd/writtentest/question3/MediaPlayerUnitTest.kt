package com.bongobd.writtentest.question3

import com.bongobd.writtentest.question3.states.StatePlaying
import com.bongobd.writtentest.question3.states.StateReady
import com.bongobd.writtentest.question3.states.StateStopped
import org.junit.Test

class MediaPlayerUnitTest {

    @Test
    fun player_readyState_to_stoppedState() {
        val player = Player()

        assert(player.playerState is StateReady)
        player.playerState.onStop()

        assert(player.playerState is StateStopped)
    }

    @Test
    fun player_readyState_to_playingState() {
        val player = Player()

        assert(player.playerState is StateReady)
        player.playerState.onPlay()

        assert(player.playerState is StatePlaying)
    }

    @Test
    fun player_playingState_to_stopState() {
        val player = Player()
        println(player.playerState.onPlay())

        assert(player.playerState is StatePlaying)
        println(player.playerState.onStop())

        assert(player.playerState is StateStopped)
    }

    @Test
    fun player_playingState_to_pauseState() {
        val player = Player()
        println(player.playerState.onPlay())

        assert(player.playerState is StatePlaying)
        println(player.playerState.onPlay())

        assert(player.playerState is StateReady)
    }

    @Test
    fun player_stoppedState_to_playingState() {
        val player = Player()
        println(player.playerState.onStop())

        assert(player.playerState is StateStopped)
        println(player.playerState.onPlay())

        assert(player.playerState is StatePlaying)
    }

    @Test
    fun player_stoppedState_to_readyState() {
        val player = Player()
        println(player.playerState.onStop())

        assert(player.playerState is StateStopped)
        println(player.playerState.onStop())

        assert(player.playerState is StateReady)
    }

    @Test
    fun player_forwardAndRewind_whileInReadyState() {
        val player = Player()

        println(player.playerState.onForward())
        println(player.playerState.onRewind())
    }

    @Test
    fun player_forwardAndRewind_whileInPlayingState() {
        val player = Player()
        player.playerState.onPlay()

        println(player.playerState.onForward())
        println(player.playerState.onRewind())
    }

    @Test
    fun player_forwardAndRewind_whileInStoppedState() {
        val player = Player()
        player.playerState.onPlay()
        player.playerState.onStop()

        println(player.playerState.onForward())
        println(player.playerState.onRewind())
    }
}