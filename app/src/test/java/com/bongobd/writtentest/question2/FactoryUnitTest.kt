package com.bongobd.writtentest.question2

import com.bongobd.writtentest.question2.factory.Car
import com.bongobd.writtentest.question2.factory.Plane
import com.bongobd.writtentest.question2.factory.VehicleFactory
import org.junit.Before
import org.junit.Test

class FactoryUnitTest {

    private val vehicleFactory = VehicleFactory()

    @Before
    fun createInstances() {
        vehicleFactory.registerInstance("Car", Car())
        vehicleFactory.registerInstance("Plane", Plane())
    }

    @Test
    fun create_carInstance() {
        assert(vehicleFactory.getInstance("Car") is Car)
    }

    @Test
    fun create_planeInstance() {
        assert(vehicleFactory.getInstance("Plane") is Plane)
    }
}