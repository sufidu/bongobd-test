package com.bongobd.writtentest.question2

import com.bongobd.writtentest.question2.abstractfactory.*
import org.junit.Assert.assertFalse
import org.junit.Test

class AbstractFactoryUnitTest {

    @Test
    fun create_vehicle_instance_from_factory() {
        val factoryProducer = FactoryProducer()

        assert(factoryProducer.createFactory("Land")?.createVehicle("Car") is Car)
        assert(factoryProducer.createFactory("Land")?.createVehicle("Bus") is Bus)
        assert(factoryProducer.createFactory("Air")?.createVehicle("Plane") is Plane)
        assert(factoryProducer.createFactory("Air")?.createVehicle("Helicopter") is Helicopter)


    }

    @Test
    fun create_vehicle_when_factory_name_is_wrong() {
        val factoryProducer = FactoryProducer()

        assertFalse(factoryProducer.createFactory("None")?.createVehicle("Car") is Car)
    }

    @Test
    fun create_vehicle_when_vehicle_type_is_wrong() {
        val factoryProducer = FactoryProducer()

        assertFalse(factoryProducer.createFactory("Air")?.createVehicle("Car") is Car)
    }
}