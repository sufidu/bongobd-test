package com.bongobd.writtentest.question1

import org.junit.Assert.assertFalse
import org.junit.Test

class AnagramUnitTest {

    @Test
    fun is_anagram() {

        val anagramFinder = AnagramFinder()

        assert(anagramFinder.isAnagram("debit card", "bad credit"))
        assert(anagramFinder.isAnagram("School master", "The classroom"))
        assert(anagramFinder.isAnagram("Conversation", " Voices   rant on  "))


        assertFalse("Anagram", anagramFinder.isAnagram("School master", "The classroo"))
        assertFalse("Anagram", anagramFinder.isAnagram("Astronomer", "Moon starar"))
    }
}