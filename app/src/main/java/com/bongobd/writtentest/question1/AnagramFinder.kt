package com.bongobd.writtentest.question1

import java.util.*

class AnagramFinder {

    fun isAnagram(originalString: String, anagramString: String): Boolean {

        // Remove all whitespace and convert strings to lowercase
        val stringOriginal = originalString.replace(" ", "").toLowerCase(Locale.US)
        val stringAnagram = anagramString.replace(" ", "").toLowerCase(Locale.US)

        // Check length of both strings. If lengths are not equal, then they are not anagram.
        if(stringOriginal.length != stringAnagram.length) return false

        // Convert strings to char array, then sort the array
        val arrayOriginal = stringOriginal.toCharArray().sortedArray()
        val arrayAnagram = stringAnagram.toCharArray().sortedArray()

        // If contents are equal then they are anagram.
        return arrayOriginal contentEquals arrayAnagram
    }
}