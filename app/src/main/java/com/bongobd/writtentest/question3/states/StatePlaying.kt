package com.bongobd.writtentest.question3.states

import com.bongobd.writtentest.question3.Player

class StatePlaying(private val player: Player) : PlayerState(player) {

    init {
        player.isPlaying = true
    }

    override fun onPlay(): String {
        player.pausePlayback()
        player.playerState = StateReady(player)
        return "Media paused"
    }

    override fun onStop(): String {
        player.stopPlayback()
        player.playerState = StateStopped(player)
        return "Media Stopped"
    }

    override fun onForward(): String {
        player.doForward()
        return "Media is Forwarding"
    }

    override fun onRewind(): String {
        player.doRewind()
        return "Media is Rewinding"
    }
}