package com.bongobd.writtentest.question3.states

import com.bongobd.writtentest.question3.Player

class StateStopped(private val player: Player) : PlayerState(player) {

    init {
        player.isPlaying = false
    }

    override fun onPlay(): String {
        player.startPlayback()
        player.playerState = StatePlaying(player)
        return "Media started playing"
    }

    override fun onStop(): String {
        player.playerState = StateReady(player)
        return "Media is ready to play"
    }

    override fun onForward(): String {
        return "Media is in STOP state and not playing"
    }

    override fun onRewind(): String {
        return "Media is in STOP state and not playing"
    }
}