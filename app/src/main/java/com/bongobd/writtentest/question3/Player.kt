package com.bongobd.writtentest.question3

import com.bongobd.writtentest.question3.states.PlayerState
import com.bongobd.writtentest.question3.states.StateReady

class Player {

    var playerState: PlayerState = StateReady(this)
    var isPlaying = false


    fun startPlayback() {
        // Do stuff to play media
    }

    fun pausePlayback() {
        // Do stuff to pause media
    }

    fun stopPlayback() {
        // Do stuff to stop media
    }

    fun doForward() {
        // Do stuff to forward media
    }

    fun doRewind() {
        // Do stuff to rewind media
    }
}