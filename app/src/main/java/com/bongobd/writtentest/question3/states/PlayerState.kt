package com.bongobd.writtentest.question3.states

import com.bongobd.writtentest.question3.Player

abstract class PlayerState(player: Player) {

    abstract fun onPlay(): String
    abstract fun onStop(): String
    abstract fun onForward(): String
    abstract fun onRewind(): String
}