package com.bongobd.writtentest.question3.states

import com.bongobd.writtentest.question3.Player

class StateReady(private val player: Player) : PlayerState(player) {

    override fun onPlay(): String {
        player.startPlayback()
        player.playerState = StatePlaying(player)
        return "Media started playing"
    }

    override fun onStop(): String {
        if(player.isPlaying) {
            player.stopPlayback()
            player.playerState = StateStopped(player)
            return "Media stopped"
        }
        return "Media already stopped"

    }

    override fun onForward(): String {
        return "Media is in READY state and not playing"
    }

    override fun onRewind(): String {
        return "Media is in READY state and not playing"
    }
}