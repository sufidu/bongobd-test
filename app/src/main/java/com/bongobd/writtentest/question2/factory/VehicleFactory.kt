package com.bongobd.writtentest.question2.factory

class VehicleFactory {

    private val instanceMap = hashMapOf<String, Vehicle>()

    fun registerInstance(vehicleType: String, instance: Vehicle) {
        instanceMap[vehicleType] = instance
    }

    fun getInstance(vehicleName: String): Vehicle? {
        return instanceMap[vehicleName]
    }
}