package com.bongobd.writtentest.question2.abstractfactory

/*
interface Vehicle {
    fun numOfWheels(): Int
    fun numOfPassengers(): Int
    fun hasGas(): Boolean
}

class Car(
    var numberOfWheels: Int = 4,
    var numberOfPassengers: Int = 5,
    var gasLevel: Int = 0
) : Vehicle {

    override fun numOfWheels(): Int {
        return numberOfWheels
    }

    override fun numOfPassengers(): Int {
        return numberOfPassengers
    }

    override fun hasGas(): Boolean {
        return gasLevel > 0
    }
}

class Bus(
    var numberOfWheels: Int = 6,
    var numberOfPassengers: Int = 40,
    var gasLevel: Int = 0
) : Vehicle {

    override fun numOfWheels(): Int {
        return numberOfWheels
    }

    override fun numOfPassengers(): Int {
        return numberOfPassengers
    }

    override fun hasGas(): Boolean {
        return gasLevel > 0
    }
}

class Plane(
    var numberOfWheels: Int = 10,
    var numberOfPassengers: Int = 120,
    var gasLevel: Int = 0
) : Vehicle {

    override fun numOfWheels(): Int {
        return numberOfWheels
    }

    override fun numOfPassengers(): Int {
        return numberOfPassengers
    }

    override fun hasGas(): Boolean {
        return gasLevel > 0
    }
}

class Helicopter(
    var numberOfWheels: Int = 6,
    var numberOfPassengers: Int = 6,
    var gasLevel: Int = 0
) : Vehicle {

    override fun numOfWheels(): Int {
        return numberOfWheels
    }

    override fun numOfPassengers(): Int {
        return numberOfPassengers
    }

    override fun hasGas(): Boolean {
        return gasLevel > 0
    }
}*/
