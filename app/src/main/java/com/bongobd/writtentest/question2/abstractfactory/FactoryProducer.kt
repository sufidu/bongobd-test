package com.bongobd.writtentest.question2.abstractfactory

class FactoryProducer {

    fun createFactory(factoryType: String) : AbstractFactory<Vehicle>? {
        return when(factoryType) {
            "Land" -> LandVehicleFactory()
            "Air" -> AirVehicleFactory()
            else -> null
        }
    }
}