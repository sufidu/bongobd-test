package com.bongobd.writtentest.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bongobd.writtentest.R
import com.bongobd.writtentest.question3.Player
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val player = Player()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvPlayerState.text = "Player is ready"
        btnPlayPause.setOnClickListener { tvPlayerState.text = player.playerState.onPlay() }
        btnStop.setOnClickListener { tvPlayerState.text = player.playerState.onStop() }
        btnForward.setOnClickListener { tvPlayerState.text = player.playerState.onForward() }
        btnRewind.setOnClickListener { tvPlayerState.text = player.playerState.onRewind() }
    }
}
